package com.example.remitanovideoshare.controller;

import com.example.remitanovideoshare.bean.CommonResponse;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/healthcheck")
public class HealthCheckController {

    @PostMapping(path = "/getTime",produces = "application/json")
    public CommonResponse getTime(){
        return new CommonResponse("200",System.currentTimeMillis());
    }
}
